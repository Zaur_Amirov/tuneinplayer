package com.zauramirov.tuneinplayer;

import android.app.Application;

/**
 * Created by Zaur on 03.02.2016.
 */
public class App extends Application {

    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);
    }

    private static void setInstance(final App app) {
        App.instance = app;
    }

    public static App getInstance() {
        return instance;
    }
}
