package com.zauramirov.tuneinplayer.models;

/**
 * Created by Zaur on 03.02.2016.
 */
public class TuneIn {

    private String title;
    private String subTitle;
    private String filePathSD;
    private String url;

    public TuneIn(final String title, final String subTitle, final String filePathSD, final String url) {
        this.title = title;
        this.filePathSD = filePathSD;
        this.url = url;
        this.subTitle = subTitle;
    }

    public void setFilePathSD(final String filePathSD) {
        this.filePathSD = filePathSD;
    }

    public String getTitle() {
        return title;
    }

    public String getFilePathSD() {
        return filePathSD;
    }

    public String getUrl() {
        return url;
    }

    public String getSubTitle() {
        return subTitle;
    }
}
