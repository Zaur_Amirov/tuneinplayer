package com.zauramirov.tuneinplayer.callbacks;

import com.zauramirov.tuneinplayer.models.TuneIn;

import java.util.List;

/**
 * Created by Zaur on 03.02.2016.
 */
public interface TuneInPodcastDownloadCallBack {
    public void onReceivePodcast (final List<TuneIn> podcastList);
    public void onReceivePodcastError (final String error);
}
