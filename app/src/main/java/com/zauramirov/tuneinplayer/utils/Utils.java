package com.zauramirov.tuneinplayer.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.zauramirov.tuneinplayer.App;
import com.zauramirov.tuneinplayer.BuildConfig;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Zaur on 03.02.2016.
 */
public class Utils {


    private static final String TAG = "TuneInPlayer";

    /**
     * show toast
     *
     * @param message - your message
     */
    public static void showToast(final String message) {
        Toast toast = Toast.makeText(App.getInstance().getApplicationContext(),
                message,
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    /**
     * DBGE == Log debug(Log.d)
     *
     * @param tag
     * @param msg
     */
    public static void logD(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Debug" : msg);
        }
    }

    /**
     * DBGE == Log errors(Log.e)
     *
     * @param tag
     * @param msg
     */
    public static void logE(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Error!" : msg);
        }
    }

    /**
     * DBGE == Log info(Log.i)
     *
     * @param tag
     * @param msg
     */
    public static void logI(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Info" : msg);
        }
    }

    /**
     * Check network connection
     */
    public static boolean isNetworkEnabled() {
        final ConnectivityManager connMan = (ConnectivityManager) App
                .getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo info = connMan.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    public static String downloadFile(final String imageURL) {

        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Utils.logE(TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return null;
        }
        File sdPath = Environment.getExternalStorageDirectory();
        sdPath = new File(sdPath.getAbsolutePath() + "/" + "ImageTuneIn");
        if (!sdPath.isDirectory())
            sdPath.mkdirs();

        String fileName = sdPath +"/"+ imageURL
                .substring(imageURL.lastIndexOf("/") + 1);

        try {
            URL url = new URL(imageURL);
            File file = new File(fileName);
            URLConnection icon = url.openConnection();
            InputStream is = icon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(5000);
            int current = 0;
            while ((current = bis.read()) != -1)
                baf.append((byte) current);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baf.toByteArray());
            fos.close();

        } catch (Exception e) {
            Utils.logE(TAG, "Error download image");
            fileName = null;
        }
        finally {
            return fileName;
        }
    }

}
