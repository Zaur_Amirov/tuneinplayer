package com.zauramirov.tuneinplayer.tasks;

import android.os.AsyncTask;

import com.zauramirov.tuneinplayer.callbacks.TuneInPodcastDownloadCallBack;
import com.zauramirov.tuneinplayer.models.TuneIn;
import com.zauramirov.tuneinplayer.utils.C;
import com.zauramirov.tuneinplayer.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Zaur on 03.02.2016.
 */
public class DownloadPodcastTask extends AsyncTask<Void, Void, Void> {

    private static final String TAG = "DownloadPodcastTask";
    private TuneInPodcastDownloadCallBack callback;
    private ArrayList<TuneIn> listTuneIn;
    private Exception exception = null;

    public DownloadPodcastTask(final TuneInPodcastDownloadCallBack callback) {
        this.callback = callback;
    }


    private String getResponse(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setReadTimeout(21000);
            conn.setConnectTimeout(21000);
            //default GET
            //conn.setRequestMethod("POST");
            //conn.setRequestProperty(" ",
            //        "application/x-www-form-urlencoded");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            conn.setRequestProperty("Accept", "application/json, text/javascript, */*; q=0.01");
            /*
            NOT USED IF responseCode == 200 with GET
             */
            //conn.setDoOutput(true);// - for POST !!!
            int responseCode = conn.getResponseCode();

            Utils.logD(TAG, "responseCode = " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            exception = e;
            e.printStackTrace();
            Utils.logD(TAG, e.getMessage());

        }
        return response;
    }


    @Override
    protected Void doInBackground(Void... params) {
        String response;
        response = getResponse(C.URL_TOP_PODCAST);
        Utils.logD(TAG, response);
        parseJSON(response);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (exception == null)
            callback.onReceivePodcast(listTuneIn);
        else
            callback.onReceivePodcastError(exception.getMessage());
    }

    private void parseJSON(final String response) {
        try {
            if (response == null | response.isEmpty()) return;
            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.has("payload")) {
                Utils.logD(TAG, "payload");
                JSONObject payload = jsonObject.getJSONObject("payload");
                if (payload.has("ContainerGuideItems")) {
                    Utils.logD(TAG, "ContainerGuideItems");
                    JSONObject containerGuideItems = payload.getJSONObject("ContainerGuideItems");
                    if (containerGuideItems.has("containers")) {
                        Utils.logD(TAG, "containers");
                        JSONArray containers = containerGuideItems.getJSONArray("containers");
                        JSONObject containersFirstElement = containers.getJSONObject(0);
                        Utils.logD(TAG, containersFirstElement.toString());
                        if (containersFirstElement.has("GuideItems")) {
                            Utils.logD(TAG, "GuideItems");
                            String title;
                            String filePath;
                            String url;
                            String subTitle;

                            JSONArray guideItems = containersFirstElement.getJSONArray("GuideItems");
                            listTuneIn = new ArrayList<TuneIn>();
                            for (int i=0; i<guideItems.length(); i++){
                                JSONObject object = guideItems.getJSONObject(i);
                                title = object.getString("Title");
                                subTitle = object.getString("Subtitle");
                                url = object.getString("Image");
                                filePath = Utils.downloadFile(url);
                                Utils.logD(TAG, title);
                                Utils.logD(TAG, url);
                                Utils.logD(TAG, filePath);
                                Utils.logD(TAG, subTitle);
                                listTuneIn.add(new TuneIn(title, subTitle, filePath, url));
                            }
                        }

                    }
                }
            }

        } catch (JSONException e) {

        }
    }
}
