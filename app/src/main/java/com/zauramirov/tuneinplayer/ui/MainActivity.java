package com.zauramirov.tuneinplayer.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.zauramirov.tuneinplayer.R;
import com.zauramirov.tuneinplayer.adapters.TuneInAdapter;
import com.zauramirov.tuneinplayer.callbacks.TuneInPodcastDownloadCallBack;
import com.zauramirov.tuneinplayer.models.TuneIn;
import com.zauramirov.tuneinplayer.tasks.DownloadPodcastTask;
import com.zauramirov.tuneinplayer.utils.Utils;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements TuneInPodcastDownloadCallBack {

    private Button btnUpdate;
    private ProgressDialog pDialog;
    private TuneInAdapter adapter;
    private ListView lvPodcast;
    private final String TAG = "MainActivity";
    private TextToSpeech mTTS;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTTS = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    //set local need in strings with resourse for app
                    mTTS.setLanguage(Locale.UK);
                } else {
                }

            }
        });

        btnUpdate = (Button) findViewById(R.id.btn_update);
        lvPodcast = (ListView) findViewById(R.id.lv_podcast);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isNetworkEnabled()) {
                    v.setEnabled(false);
                    pDialog = new ProgressDialog(MainActivity.this);
                    pDialog.setTitle(getString(R.string.wait));
                    pDialog.setCancelable(false);
                    pDialog.show();
                    DownloadPodcastTask taskDownload = new DownloadPodcastTask(MainActivity.this);
                    taskDownload.execute();
                } else {
                    Utils.showToast(getString(R.string.no_internet));
                }
            }
        });

    }

    @Override
    public void onReceivePodcast(final List<TuneIn> podcastList) {
        btnUpdate.setEnabled(true);
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
        adapter = new TuneInAdapter(MainActivity.this, podcastList, mTTS);
        lvPodcast.setAdapter(adapter);
        lvPodcast.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Utils.logD(TAG, "position = " + position);
            }
        });

    }

    @Override
    public void onReceivePodcastError(final String error) {
        btnUpdate.setEnabled(true);
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
        Utils.showToast(error);
    }

    @Override
    public void onStop() {
        if (mTTS != null) {
            mTTS.stop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mTTS != null) {
            mTTS.shutdown();
        }
        super.onDestroy();
    }
}
