package com.zauramirov.tuneinplayer.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.zauramirov.tuneinplayer.R;
import com.zauramirov.tuneinplayer.models.TuneIn;

import java.io.File;
import java.util.List;

/**
 * Created by Zaur on 03.02.2016.
 */
public class TuneInAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<TuneIn> objects;
    private TextToSpeech mTTS;

    public TuneInAdapter(final Context context, final List<TuneIn> items, final TextToSpeech mTTS) {
        this.objects = items;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mTTS = mTTS;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public TuneIn getItem(int position) {
        return objects.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_tunein, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textView = (TextView) convertView.findViewById(R.id.tv_title);
            viewHolder.imageButton = (ImageButton) convertView.findViewById(R.id.ib_play);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.iv_icon);

            viewHolder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTTS.speak(objects.get(position).getSubTitle(), TextToSpeech.QUEUE_ADD, null);
                }
            });

            viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTTS.speak(objects.get(position).getSubTitle(), TextToSpeech.QUEUE_ADD, null);
                }
            });

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textView.setText(objects.get(position).getTitle());
        String icon = objects.get(position).getFilePathSD();

        File imageFile = new File(icon);
        //if not file, for example, we delete it, we download file now
        if (imageFile.exists()) {
            viewHolder.imageView.setImageBitmap(BitmapFactory.decodeFile(imageFile.getAbsolutePath()));
        } else {
            //TODO need create download icon and set in imageView
        }

        return convertView;
    }

    static class ViewHolder {
        TextView textView;
        ImageView imageView;
        ImageButton imageButton;
    }
}
